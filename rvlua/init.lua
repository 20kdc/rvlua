--[[
 rvlua - Attempt to efficiently run RISC-V code in Lua.
 Written starting in 2018 by contributors (see CONTRIBUTORS.txt).
 To the extent possible under law, the author(s) have dedicated all copyright and related and neighboring rights to this software to the public domain worldwide. This software is distributed without any warranty.
 You should have received a copy of the CC0 Public Domain Dedication along with this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
--]]

-- Attempt at an implementation of a RISC-V JIT.
-- Why? Faster virtual computers, of course.
-- Needs a bit32 implementation and riscv.int64
-- By default, supports just RV32I user specification & little else.
-- Can add the 'M' extension to help deal with
--  missing functionality, leading to a RV32IM processor.
-- Hopefully easy enough to extend (CAFDQ & beyond???)

local bit32
local bitBand, bitBor, bitBxor, bitLS, bitRS, bitRSL
local unpack = unpack or table.unpack

local function i64Add32(h, l, m)
	l = l + m
	h = bitBand(h + math.floor(l / 0x100000000), 0xFFFFFFFF)
	l = bitBand(l, 0xFFFFFFFF)
	return h, l
end

-- Instruction Format Decoders

-- Returns rd, funct3, rs1, imm, rs2, funct7
local function decodeRIType(op)
	local rd = bitBand(bitRS(op, 7), 0x1F)
	local funct3 = bitBand(bitRS(op, 12), 0x07)
	local rs1 = bitBand(bitRS(op, 15), 0x1F)
	-- bitRS performs the necessary sign-extension here.
	local imm = bitRS(op, 20)
	local rs2 = bitBand(bitRS(op, 20), 0x1F)
	local funct7 = bitBand(bitRS(op, 25), 0x7F)
	return rd, funct3, rs1, imm, rs2, funct7
end

-- Returns rd, funct3, rs1, rs2, funct7
local function decodeRType(op)
	local rd, funct3, rs1, imm, rs2, funct7 = decodeRIType(op)
	return rd, funct3, rs1, rs2, funct7
end
-- Returns rd, funct3, rs1, rs2, funct2, rs3
local function decodeR4Type(op)
	local rd, funct3, rs1, imm, rs2, funct7 = decodeRIType(op)
	local funct2, rs3 = bitBand(funct7, 3), bitRS(funct7, 2)
	return rd, funct3, rs1, rs2, funct2, rs3
end
-- Returns rd, funct3, rs1, imm
local function decodeIType(op)
	local rd, funct3, rs1, imm = decodeRIType(op)
	return rd, funct3, rs1, imm
end

-- Returns imm, funct3, rs1, rs2
local function decodeSType(op)
	local imm = bitBand(bitRS(op, 7), 0x1F)
	imm = bitBor(imm, bitBand(bitRS(op, 20), 0xFFFFFFE0)) -- sign-extends
	-- Shared with SB
	local funct3 = bitBand(bitRS(op, 12), 0x07)
	local rs1 = bitBand(bitRS(op, 15), 0x1F)
	local rs2 = bitBand(bitRS(op, 20), 0x1F)
	return imm, funct3, rs1, rs2
end
-- Returns imm, funct3, rs1, rs2
local function decodeBType(op)
	local imm = bitLS(bitBand(op, 0x80), 4)
	imm = bitBor(imm, bitRS(bitBand(op, 0xF00), 7))
	imm = bitBor(imm, bitRS(bitBand(op, 0x7E000000), 20))
	-- This does the sign-extend
	imm = bitBor(imm, bitRS(bitBand(op, 0x80000000), 19))
	-- Shared with SB
	local funct3 = bitBand(bitRS(op, 12), 0x07)
	local rs1 = bitBand(bitRS(op, 15), 0x1F)
	local rs2 = bitBand(bitRS(op, 20), 0x1F)
	return imm, funct3, rs1, rs2
end
-- Returns rd, imm
local function decodeUType(op)
	local imm = bitBand(op, 0xFFFFF000)
	return bitBand(bitRS(op, 7), 0x1F), imm
end
-- Returns rd, imm
local function decodeJType(op)
	-- Note: This is a bit *left*, so this is always a multiple of 2
	local imm = bitBand(bitRS(op, 20), 0x7FE)
	imm = bitBor(imm, bitBand(bitRS(op, 9), 0x800))
	imm = bitBor(imm, bitBand(op, 0xFF000))
	-- This does the sign-extend
	imm = bitBor(imm, bitRS(bitBand(op, 0x80000000), 11))
	return bitBand(bitRS(op, 7), 0x1F), imm
end
--

return {
	decodeRIType = decodeRIType,
	decodeRType = decodeRType,
	decodeR4Type = decodeR4Type,
	decodeIType = decodeIType,
	decodeSType = decodeSType,
	decodeBType = decodeBType,
	decodeUType = decodeUType,
	decodeJType = decodeJType,
	set_bit32 = function (b32)
		bit32 = b32
		bitBand = b32.band
		bitBor = b32.bor
		bitBxor = b32.bxor
		bitRS = b32.arshift
		bitRSL = b32.rshift
		bitLS = b32.lshift
	end,
	get_bit32 = function ()
		return bit32
	end,
	-- NOTE! Memory and bit32 are local-locked for performance reasons.
	-- Memory is something of a semi-subset of the memlib interface -
	--  it WILL receive unaligned accesses, but accesses must fail by returning nil
	-- (this includes writes)
	-- options.memlibMode removes the error checks, so you need to use error and doing so will corrupt the CPU state.
	-- Exact exception semantics are do-this-later right now though,
	--  as a workingish CPU with bad exception handling will still run C
	-- options.autoInstret enables proper handling of instret at a performance cost.
	new = function (options, codeG32, memoryG32, memoryG16, memoryG8, memoryS32, memoryS16, memoryS8)
		local regs = {}
		local cpu = {regs = regs}
		-- Cannot hook
		cpu.memory = {
			codeG32 = codeG32,
			memoryG32 = memoryG32,
			memoryG16 = memoryG16,
			memoryG8 = memoryG8,
			memoryS32 = memoryS32,
			memoryS16 = memoryS16,
			memoryS8 = memoryS8
		}
		-- CPU 64-bit counters, big-endian. Nilling these disables them.
		-- Only instret is automatically adjusted, and only if specified.
		cpu.cycle =   {0x00000000, 0x00000000}
		cpu.time =    {0x00000000, 0x00000000}
		cpu.instret = {0x00000000, 0x00000000}
		-- CPU runtime callbacks.
		cpu.trap = {
			access = {
				codeG32 = function ()
					error("Invalid Instruction Address 0x" .. string.format("%08x", cpu.pc))
				end,
				dataG32 = function (target)
					error("Invalid Word Read 0x" .. string.format("%08x", target))
				end,
				dataS32 = function (target, value)
					error("Invalid Word Write 0x" .. string.format("%08x", target))
				end,
				dataG16 = function (target)
					error("Invalid Half Read 0x" .. string.format("%08x", target))
				end,
				dataS16 = function (target, value)
					error("Invalid Half Write 0x" .. string.format("%08x", target))
				end,
				dataG8 = function (target)
					error("Invalid Byte Read 0x" .. string.format("%08x", target))
				end,
				dataS8 = function (target, value)
					error("Invalid Byte Write 0x" .. string.format("%08x", target))
				end,
				-- NOTE! These two access traps can actually not error, and return valid values.
				-- The sequence is always csrG32, csrS32, pipeline break.
				-- Either of the csr calls can be omitted, but no more can be added or reordered.
				-- So to make things simpler, returning nil from csrG32 causes an immediate pipeline break before the csrS32.
				-- And something truthy returned from csrS32 causes cpu.pc to not get updated.
				-- Note that in order to comply with atomicity, the csrS32 trap must return for the register update from the csrG32.
				csrG32 = function (ins, target)
					if target == 0xC00 and cpu.cycle then
						return cpu.cycle[2]
					elseif target == 0xC01 and cpu.time then
						return cpu.time[2]
					elseif target == 0xC02 and cpu.instret then
						return cpu.instret[2]
					elseif target == 0xC80 and cpu.cycle then
						return cpu.cycle[1]
					elseif target == 0xC81 and cpu.time then
						return cpu.time[1]
					elseif target == 0xC82 and cpu.instret then
						return cpu.instret[1]
					end
					cpu.trap.subop(ins, "Invalid CSR Read 0x" .. string.format("%08x", target))
				end,
				csrS32 = function (ins, target, value)
					cpu.trap.subop(ins, "Invalid CSR Write 0x" .. string.format("%08x", target))
				end
			},
			opcode = function (id, ins)
				error("Invalid Opcode 0x" .. string.format("%02x", id))
			end,
			subop = function (ins, estr)
				error(estr)
			end,
			ecall = function (pcAdvance)
				error("ECALL @ 0x" .. string.format("%08x", cpu.pc))
			end,
			ebreak = function (pcAdvance)
				error("EBREAK @ 0x" .. string.format("%08x", cpu.pc))
			end
		}
		--
		local regFill = ""
		for i = 1, 31 do
			regs[i] = 0
			if i ~= 1 then
				regFill = regFill .. ", r" .. i
			else
				regFill = regFill .. "r" .. i
			end
		end
		cpu.pc = 0
		-- [startAddress] = function (r, regs...)
		--  NOTE! This is not a stable API.
		cpu.icache = {}
		-- [opcode] = function (reguse, ins) -> code
		--  NOTE! This is not a stable API.
		cpu.opcomps = {}
		cpu.copyReguse = function (reguse)
			local n = {}
			for k, v in pairs(reguse) do
				n[k] = v
			end
			return n
		end
		cpu.advancePC = function (reguse)
			reguse.pc = bitBand(reguse.pc + reguse.pcAdvance, 0xFFFFFFFF)
		end
		-- pc here can be nil if you just want to flush registers (for operations with unknown target PC)
		-- 'partial' is for conditional code, and if used, no flushes must occur between this and a return.
		cpu.compileFlush = function (reguse, pc, partial)
			local code = (pc and ("cpu.pc = " .. reguse.pc .. " ")) or ""
			-- Note that r0 never gets flushed.
			for k = 1, 31 do
				if reguse[k] == "w" then
					code = code .. "regs[" .. k .. "] = r" .. k .. " "
					if not partial then
						reguse[k] = "r"
					end
				end
			end
			if options.autoInstret then
				-- Update instructions retired
				code = code .. "cpu.instret[1], cpu.instret[2] = i64Add32(cpu.instret[1], cpu.instret[2], " .. reguse.instret .. ") "
				if not partial then
					reguse.instret = 0
				end
			end
			return code
		end

		if not (loadstring or load)("return 1 & 0xFFFFFFFF") then
			-- Most Lua versions
			cpu.compileBandFull = function (a)
				return "bitBand(" .. a .. ", 0xFFFFFFFF)"
			end
		else
			-- Lua 5.3 bitops
			cpu.compileBandFull = function (a)
				return "((" .. a .. ") & 0xFFFFFFFF)"
			end
		end

		-- NOTE! CSRR support is dependent on the register caching for atomicity.
		-- If you disable the register caching for whatever awful reason, go to compileCSRR and add in a temporary local.		
		cpu.compileRegGet = function (reguse, k)
			if k == 0 then
				return "", "0"
			end
			if not reguse[k] then
				reguse[k] = "r"
				return "local r" .. k .. " = regs[" .. k .. "] ", "r" .. k
			end
			return "", "r" .. k
		end
		cpu.compileRegSet = function (reguse, k)
			if k == 0 then
				return "", "_"
			end
			if not reguse[k] then
				reguse[k] = "w"
				return "local r" .. k .. " ", "r" .. k
			end
			reguse[k] = "w"
			return "", "r" .. k
		end
		cpu.cmgShifter = function (expr, lsh)
			if not lsh then
				return expr
			end
			return "bitRS(bitLS(" .. expr .. ", " .. lsh .. "), " .. lsh .. ")"
		end
		if options.memlibMode then
			-- Returns code: start .. "memoryS16(0, 0)" .. end
			cpu.compileMemcheckSet = function (reguse, tp, kexpr, vexpr)
				return "memory" .. tp .. "(" .. kexpr .. ", " .. vexpr .. ") "
			end
			-- Returns code.
			cpu.compileMemcheckGet = function (reguse, vexpr, tp, kexpr, lsh)
				return vexpr .. " = " .. cpu.cmgShifter("memory" .. tp .. "(" .. kexpr .. ")", lsh) .. " "
			end
		else
			-- Returns code: start .. "memoryS16(0, 0)" .. end
			cpu.compileMemcheckSet = function (reguse, tp, kexpr, vexpr)
				return "if not memory" .. tp .. "(" .. kexpr .. ", " .. vexpr .. ") then " .. cpu.compileFlush(reguse, true, true) .. "cpu.trap.access.data" .. tp .. "(" .. kexpr .. ", " .. vexpr .. ") return end "
			end
			-- Returns code.
			cpu.compileMemcheckGet = function (reguse, vexpr, tp, kexpr, lsh)
				return "_ = memory" .. tp .. "(" .. kexpr .. ") if not _ then " .. cpu.compileFlush(reguse, true, true) .. "cpu.trap.access.data" .. tp .. "(" .. kexpr .. ") return end " .. vexpr .. " = " .. cpu.cmgShifter("_", lsh) .. " "
			end
		end
		cpu.compilePIM = function (code, imm)
			if imm == 0 then
				return code
			end
			return cpu.compileBandFull(code .. "+" .. imm)
		end
		cpu.compileTrap = function (reguse, trapcore)
			local code = cpu.compileFlush(reguse, true)
			reguse.pc = nil
			return code .. trapcore
		end
		cpu.compileSubopTrap = function (reguse, ins, estr)
			return cpu.compileTrap(reguse, "cpu.trap.subop(" .. ins .. ", " .. estr .. ") ")
		end
		cpu.compileThread = function (reguse)
			local code = ""
			local firstpc = reguse.pc
			while reguse.quotaI > 0 do
				reguse.quotaI = reguse.quotaI - 1
				if options.autoInstret then
					reguse.instret = reguse.instret + 1
				end
				reguse.unroll = reguse.quotaI >= reguse.unrollIQF
				reguse.pcAdvance = 4
				-- All statements must end with a space.
				local ins = codeG32(reguse.pc)
				if debug then
					code = code .. "\n--[[ " .. string.format("%08x %08x %s", reguse.pc, ins, tostring(reguse.unroll)) .. " --]] "
				end
				if not ins then
					code = code .. cpu.compileTrap(reguse, "cpu.trap.access.code(" .. reguse.pc .. ") ")
					reguse.pc = nil
				else
					local opcode = bitBand(ins, 0x7F)
					if not cpu.opcomps[opcode] then
						code = code .. cpu.compileTrap(reguse, "cpu.trap.opcode(" .. opcode .. ", " .. ins .. ") ")
					else
						local ncode
						ncode = cpu.opcomps[opcode](reguse, ins)
						code = code .. ncode
					end
				end
				-- The (pc == reguse.pc) detects infinite loops, but the unroll check
				--  allows some loop unrolling to occur without sacrificing too much icache on start position variations.
				if (not reguse.pc) or ((firstpc == reguse.pc) and not reguse.unroll) then
					break
				end
			end
			if reguse.pc then
				code = code .. cpu.compileFlush(reguse, true)
			end
			return code
		end
		-- function (pc) -> function (r, regs...)
		--  NOTE! This is not a stable API.
		cpu.compile = function (pc, maxi, maxb, debug)
			local code = "local cpu, regs, i64Add32, bitBand, bitBor, bitBxor, bitLS, bitRS, bitRSL, memoryG32, memoryG16, memoryG8, memoryS32, memoryS16, memoryS8, _ = ... return function ()\n"
			-- 'reguse' is global state
			local reguse = {
				-- Set to nil in an opcode compiler to indicate that the compile must end now with no flush
				pc = pc,
				instret = (options.autoInstret and 0) or nil,
				quotaI = maxi,
				unrollIQF = math.floor(maxi / 2),
				quotaB = maxb,
				[0] = "r" -- x0 access
			}
			code = code .. cpu.compileThread(reguse)
			code = code .. "end"
			if debug then print(code) end
			return (loadstring or load)(code)(cpu, regs, i64Add32, bitBand, bitBor, bitBxor, bitLS, bitRS, bitRSL, memoryG32, memoryG16, memoryG8, memoryS32, memoryS16, memoryS8)
		end
		-- Move forward a single instruction.
		cpu.step = function (debug)
			cpu.compile(cpu.pc, 1, 0, debug)(unpack(regs))
		end
		-- Move forward one icache block.
		cpu.leap = function (amt, amb, debug)
			if not cpu.icache[cpu.pc] then
				cpu.icache[cpu.pc] = cpu.compile(cpu.pc, amt, amb, debug)
			end
			cpu.icache[cpu.pc](unpack(regs))
		end
		-- CPU instruction set major components
		-- actimm is to prevent accidental undocumented instructions
		cpu.compileALU = function (funct3, funct7, rs1cc, rs2cc, actimm)
			if funct3 == 0 and funct7 == 0 then
				-- ADD
				return cpu.compileBandFull(rs1cc .. " + " .. rs2cc)
			elseif funct3 == 0 and funct7 == 32 then
				-- SUB
				return cpu.compileBandFull(rs1cc .. " - " .. rs2cc)
			elseif funct3 == 1 and funct7 == 0 then
				-- SLL
				return "bitLS(" .. rs1cc .. ", bitBand(" .. rs2cc .. ", 0x1F))"
			elseif funct3 == 2 and funct7 == 0 then
				-- SLT
				return "((bitBxor(" .. rs1cc .. ", 0x80000000) < bitBxor(" .. rs2cc .. ", 0x80000000)) and 1) or 0"
			elseif funct3 == 3 and funct7 == 0 then
				-- SLTU
				return "((" .. rs1cc .. " < " .. rs2cc .. ") and 1) or 0"
			elseif funct3 == 4 and funct7 == 0 then
				-- XOR
				return "bitBxor(" .. rs1cc .. ", " .. rs2cc .. ")"
			elseif funct3 == 5 and funct7 == 0 then
				-- SRL
				return "bitRSL(" .. rs1cc .. ", bitBand(" .. rs2cc .. ", 0x1F))"
			elseif funct3 == 5 and funct7 == 32 then
				-- SRA
				return "bitRS(" .. rs1cc .. ", bitBand(" .. rs2cc .. ", 0x1F))"
			elseif funct3 == 6 and funct7 == 0 then
				-- OR
				return "bitBor(" .. rs1cc .. ", " .. rs2cc .. ")"
			elseif funct3 == 7 and funct7 == 0 then
				-- AND
				return "bitBand(" .. rs1cc .. ", " .. rs2cc .. ")"
			else
				return
			end
		end
		-- funct3 can be 1 (CSRRW), 2 (CSRRS), or 3 (CSRRC)
		-- rs1cc can be nil to disable write, but this is only allowed by spec for 2 & 3
		cpu.compileCSRR = function (reguse, ins, rd, funct3, rs1cc, imm)
			-- This attempts to condense the CSR logic down into the critical common behaviors specified,
			--  combined with a pair of traps in order to abstract these common behaviors and furthermore
			--  allow for saner implementation unimpeded by the context of the JIT system.
			-- Do CSR trap preflush
			local code = cpu.compileFlush(reguse, true)
			if funct3 ~= 1 or rd ~= 0 then
				local rdpf, rdcc = cpu.compileRegSet(reguse, rd)
				code = code .. rdpf .. "_ = cpu.trap.csrG32(" .. ins .. ", " .. imm .. ") if not _ then return end "
				if rdcc ~= "_" then code = code .. rdcc .. " = _" end
			end
			-- '_' used as holding local for this next bit
			-- rs1cc can be nil, in which
			local expr = rs1cc
			cpu.advancePC(reguse)
			if expr then
				if funct3 == 2 then
					expr = "bitBor(_, " .. rs1cc .. ")"
				elseif funct3 == 3 then
					expr = "bitBand(_, bitBxor(" .. rs1cc .. ", 0xFFFFFFFF))"
				end
				code = code .. "cpu.pc = cpu.trap.csrS32(" .. ins .. ", " .. imm .. ", " .. expr.. ") or " .. reguse.pc .. " "
			else
				code = code .. "cpu.pc = " .. reguse.pc .. " "
			end
			reguse.pc = nil
			return code .. cpu.compileFlush(reguse, true)
		end
		-- CPU instruction set
		cpu.opcomps[0x03] = function (reguse, ins)
			-- LB/LH/LW/LBU/LHU
			local rd, funct3, rs1, imm = decodeIType(ins)
			local rs1pf, rs1cc = cpu.compileRegGet(reguse, rs1)
			local rdpf, rdcc = cpu.compileRegSet(reguse, rd)

			if funct3 == 0 then
				-- LB
				cpu.advancePC(reguse)
				return rs1pf .. rdpf .. cpu.compileMemcheckGet(reguse, rdcc, "G8", cpu.compilePIM(rs1cc, imm), 24)
			elseif funct3 == 1 then
				-- LH
				cpu.advancePC(reguse)
				return rs1pf .. rdpf .. cpu.compileMemcheckGet(reguse, rdcc, "G16", cpu.compilePIM(rs1cc, imm), 16)
			elseif funct3 == 2 then
				-- LW
				cpu.advancePC(reguse)
				return rs1pf .. rdpf .. cpu.compileMemcheckGet(reguse, rdcc, "G32", cpu.compilePIM(rs1cc, imm))
			elseif funct3 == 4 then
				-- LBU
				cpu.advancePC(reguse)
				return rs1pf .. rdpf .. cpu.compileMemcheckGet(reguse, rdcc, "G8", cpu.compilePIM(rs1cc, imm))
			elseif funct3 == 5 then
				-- LHU
				cpu.advancePC(reguse)
				return rs1pf .. rdpf .. cpu.compileMemcheckGet(reguse, rdcc, "G16", cpu.compilePIM(rs1cc, imm))
			else
				return cpu.compileSubopTrap(reguse, ins, "\"Load Family " .. funct3 .. "\"")
			end
		end
		cpu.opcomps[0x0F] = function (reguse, ins)
			local rd, funct3, rs1, imm = decodeIType(ins)
			-- The unused fields imm[11:8] (but we're allowed to ignore all of imm if we want if we assume the maximum ordering constraint level), rs1, and rd
			--  must be ignored according to the RISC-V Specification V2.2, RV32I Base Integer Instruction Set, Version 2.0, Page 20, 'The unused fields in the FENCE instruction'
			-- Later on, it also describes the same thing for FENCE.I, but it's not imm[11:8], it's just the whole of imm.
			if funct3 == 0 then
				-- FENCE
				-- The simplest implementation of this is to make it redundant (no data cache/shared between HARTs/etc.)
				-- The second simplest implementation is to simply flush everything in the predecessor set.
				-- The final implementation idea is to have a set of relations dictating what operations need to cause a flush of what operations.
				-- This does none of those.
				cpu.advancePC(reguse)
				return ""
			elseif funct3 == 1 then
				-- FENCE.I
				-- Nuke the icache.
				cpu.advancePC(reguse)
				local npc = reguse.pc
				-- Let it autoflush
				reguse.pc = nil
				return "cpu.icache = {} cpu.pc = " .. npc .. " "
			else
				return cpu.compileSubopTrap(reguse, ins, "\"MEM-MISC " .. funct3 .. "\"")
			end
		end
		cpu.opcomps[0x13] = function (reguse, ins)
			local rd, funct3, rs1, imm, rs2, funct7 = decodeRIType(ins)
			local rs1pf, rs1cc = cpu.compileRegGet(reguse, rs1)
			local rdpf, rdcc = cpu.compileRegSet(reguse, rd)
			local expr
			if funct3 == 0 or funct3 == 2 or funct3 == 3 or funct3 == 4 or funct3 == 6 or funct3 == 7 then
				-- ADDI/SLTI/SLTIU/XORI/ORI/ANDI
				expr = cpu.compileALU(funct3, 0, rs1cc, imm, true)
			elseif funct3 == 1 or funct3 == 5 then
				-- SLLI/SRLI/SRAI
				expr = cpu.compileALU(funct3, funct7, rs1cc, rs2, true)
			end
			if not expr then
				return cpu.compileSubopTrap(reguse, ins, "\"ALU Imm Family " .. funct3 .. "\"")
			end
			cpu.advancePC(reguse)
			return rs1pf .. rdpf .. rdcc .. " = " .. expr .. " "
		end
		cpu.opcomps[0x17] = function (reguse, ins)
			-- AUIPC
			local rd, imm = decodeUType(ins)
			local rdpf, rdcc = cpu.compileRegSet(reguse, rd)
			local code = rdpf .. rdcc .. " = " .. bitBand(reguse.pc + imm, 0xFFFFFFFF) .. " "
			cpu.advancePC(reguse)
			return code
		end
		cpu.opcomps[0x23] = function (reguse, ins)
			-- SB/SH/SW
			local imm, funct3, rs1, rs2 = decodeSType(ins)
			local rs1pf, rs1cc = cpu.compileRegGet(reguse, rs1)
			local rs2pf, rs2cc = cpu.compileRegGet(reguse, rs2)
			local memtype
			if funct3 == 0 then
				-- SB
				memtype = "S8"
			elseif funct3 == 1 then
				-- SH
				memtype = "S16"
			elseif funct3 == 2 then
				-- SW
				memtype = "S32"
			else
				return cpu.compileSubopTrap(reguse, ins, "\"Store Family " .. funct3 .. "\"")
			end
			cpu.advancePC(reguse)
			return rs1pf .. rs2pf .. cpu.compileMemcheckSet(reguse, memtype, cpu.compilePIM(rs1cc, imm), rs2cc)
		end
		cpu.opcomps[0x33] = function (reguse, ins)
			local rd, funct3, rs1, rs2, funct7 = decodeRType(ins)
			cpu.advancePC(reguse)

			local rs1pf, rs1cc = cpu.compileRegGet(reguse, rs1)
			local rs2pf, rs2cc = cpu.compileRegGet(reguse, rs2)
			local rdpf, rdcc = cpu.compileRegSet(reguse, rd)
			local expr = cpu.compileALU(funct3, funct7, rs1cc, rs2cc, false)
			if not expr then
				return cpu.compileSubopTrap(reguse, ins, "\"ALU Main Family " .. funct3 .. " " .. funct7 .. "\"")
			end
			return rs1pf .. rs2pf .. rdpf .. rdcc .. " = " .. expr .. " "
		end
		cpu.opcomps[0x37] = function (reguse, ins)
			-- LUI
			local rd, imm = decodeUType(ins)
			local rdpf, rdcc = cpu.compileRegSet(reguse, rd)
			local code = rdpf .. rdcc .. " = " .. imm .. " "
			cpu.advancePC(reguse)
			return code
		end
		cpu.opcomps[0x63] = function (reguse, ins)
			-- BEQ/BNE/BLT/BGE/BLTU/BGEU
			local imm, funct3, rs1, rs2 = decodeBType(ins)
			local rs1pf, rs1cc = cpu.compileRegGet(reguse, rs1)
			local rs2pf, rs2cc = cpu.compileRegGet(reguse, rs2)
			local compare
			if funct3 == 0 then
				-- BEQ
				compare = rs1cc .. " == " .. rs2cc
			elseif funct3 == 1 then
				-- BNE
				compare = rs1cc .. " ~= " .. rs2cc
			elseif funct3 == 4 then
				-- BLT
				compare = "bitBxor(" .. rs1cc .. ", 0x80000000) < bitBxor(" .. rs2cc .. ", 0x80000000)"
			elseif funct3 == 5 then
				-- BGE
				compare = "bitBxor(" .. rs1cc .. ", 0x80000000) >= bitBxor(" .. rs2cc .. ", 0x80000000)"
			elseif funct3 == 6 then
				-- BLTU
				compare = rs1cc .. " < " .. rs2cc
			elseif funct3 == 7 then
				-- BGEU
				compare = rs1cc .. " >= " .. rs2cc
			else
				return cpu.compileSubopTrap(reguse, ins, "\"Branch Family " .. funct3 .. "\"")
			end
			-- Quota is decremented in all cases to avoid abuse.
			reguse.quotaB = reguse.quotaB - 1
			-- The branch contents work in a separate reguse context.
			local code = rs1pf .. rs2pf .. "if " .. compare .. " then "
			local reguseBranchTaken = cpu.copyReguse(reguse)
			reguseBranchTaken.pc = bitBand(reguse.pc + imm, 0xFFFFFFFF)
			if reguseBranchTaken.quotaB < 0 then
				reguseBranchTaken.quotaI = 0
			end
			-- Compile the branch-taken thread
			code = code .. cpu.compileThread(reguseBranchTaken)
			-- Update PC of main thread
			cpu.advancePC(reguse)
			return code .. " return end "
		end
		cpu.opcomps[0x67] = function (reguse, ins)
			-- JALR
			local rd, funct3, rs1, imm = decodeIType(ins)
			local rs1pf, rs1cc = cpu.compileRegGet(reguse, rs1)
			local code = rs1pf .. "local npc = " .. cpu.compilePIM(rs1cc, imm) .. " "
			cpu.advancePC(reguse)
			if rd ~= 0 then
				local rdpf, rdcc = cpu.compileRegSet(reguse, rd)
				code = code .. rdpf .. rdcc .. " = " .. reguse.pc .. " "
			end
			reguse.pc = nil
			code = code .. "cpu.pc = npc "
			code = code .. cpu.compileFlush(reguse, false)
			return code
		end
		cpu.opcomps[0x6F] = function (reguse, ins)
			-- JAL
			local rd, imm = decodeJType(ins)
			local code
			local oldpc = reguse.pc
			-- Just an optimization
			if rd ~= 0 then
				local rdpf, rdcc = cpu.compileRegSet(reguse, rd)
				cpu.advancePC(reguse)
				code = rdpf .. rdcc .. " = " .. reguse.pc.. " "
			else
				code = ""
			end
			reguse.pc = bitBand(oldpc + imm, 0xFFFFFFFF)
			if not reguse.unroll then
				-- Flush (PC will be lost so autoflush won't work) & return
				code = code .. cpu.compileFlush(reguse, true)
				reguse.pc = nil
			end
			return code
		end
		cpu.opcomps[0x73] = function (reguse, ins)
			-- SYSTEM
			local rd, funct3, rs1, imm = decodeIType(ins)
			if funct3 == 1 or funct == 2 or funct == 3 then
				-- CSRRW/CSRRS/CSRRC
				local rs1pf, rs1cc = "" -- deliberate nil
				-- This is another one of those very specific wording things.
				-- CSRRS and CSRRC in particular will only write if their source isn't r0.
				if funct3 == 1 or rs1 ~= 0 then
					rs1pf, rs1cc = cpu.compileRegGet(reguse, rs1)
				end
				return rs1pf .. cpu.compileCSRR(reguse, ins, rd, funct3, rs1cc, imm)
			elseif funct3 == 5 or funct == 6 or funct == 7 then
				-- CSRRWI/CSRRSI/CSRRCI (gets remapped to CSRRW/etc. but no register access)
				return cpu.compileCSRR(reguse, ins, rd, funct3 - 4, tostring(rs1), imm)
			elseif rd == 0 and funct3 == 0 and rs1 == 0 and imm == 0 then
				if imm == 0 then
					-- ECALL
					return cpu.compileTrap(reguse, "cpu.trap.ecall(" .. reguse.pcAdvance .. ") ")
				elseif imm == 1 then
					-- EBREAK
					return cpu.compileTrap(reguse, "cpu.trap.ebreak(" .. reguse.pcAdvance .. ") ")
				end
			end
			return cpu.compileSubopTrap(reguse, ins, "\"SYSTEM family " .. funct3 .. "\"")
		end
		--
		return cpu
	end,
	-- Adds the 'M' extension to an emulated processor.
	-- The "undocumented" flag adds instructions which are NOT specified by RISC-V,
	--  but are probably going to end up in processors anyway by accident.
	extendM = function (cpu, undocumented)
		-- Multiplies two unsigned 32-bit integers, returning high/low 64-bit result
		local function muli32o64(a, b)
			local vh = math.floor(a / 0x10000) * b
			local vl = (a % 0x10000) * b
			return bitBand(vh + vl, 0xFFFFFFFF), bitBand(((vh % 0x10000) * 0x10000) + vl, 0xFFFFFFFF)
		end
		-- Multiplies two unsigned 64-bit integers, returning high/low 64-bit result
		local function muli64o64(ah, al, bh, bl)
			-- Formatting here is deliberate.
			-- The amount of 'high' sides indicates how much left-shifting is required,
			-- As all effects of XXXXXXXX00000000 x YYYYYYYY00000000 must leave the work area,
			--  this multiplication is not performed.
			local      oh1, ol1 = muli32o64(al, bl)
			local ___, oh2      = muli32o64(al, bh)
			local ___, oh3      = muli32o64(ah, bl)
			return bitBand(oh1 + oh2 + oh3, 0xFFFFFFFF), ol1
		end
		-- MUL : A? B? lower (signage doesn't affect this)
		cpu.mdr0 = function (a, b)
			local v, _ = muli32o64(a, b)
			return v
		end
		-- MULH : As Bs upper
		cpu.mdr1 = function (al, bl)
			local ah, bh = 0, 0
			if al >= 0x80000000 then
				ah = 0xFFFFFFFF
			end
			if bl >= 0x80000000 then
				bh = 0xFFFFFFFF
			end
			local oh, ol = muli64o64(ah, al, bh, bl)
			return oh
		end
		-- MULHSU: As Bu upper
		cpu.mdr2 = function (al, bl)
			local ah = 0
			if al >= 0x80000000 then
				ah = 0xFFFFFFFF
			end
			local oh, ol = muli64o64(ah, al, 0, bl)
			return oh
		end
		-- MULHU: Au Bu upper
		cpu.mdr3 = function (al, bl)
			local _, v = muli32o64(a, b)
			return v
		end
		-- And now for division. Here's where things get interesting.
		local function signed(a)
			if a >= 0x80000000 then
				return a - 0x100000000
			end
			return a
		end
		-- NOTE: For signed overflow conditions (0x80000000, -1)
		--        this logic seems to naturally return the correct answer.
		-- DIV: Signed division
		cpu.mdr4 = function (a, b)
			if b == 0 then return 0xFFFFFFFF end
			local abr = signed(a) / signed(b)
			if abr < 0 then
				abr = math.ceil(abr)
			else
				abr = math.floor(abr)
			end
			--print(signed(a), signed(b), abr)
			return bitBand(abr, 0xFFFFFFFF)
		end
		-- DIVU: Unsigned division
		cpu.mdr5 = function (a, b)
			if b == 0 then return 0xFFFFFFFF end
			return bitBand(math.floor(a / b), 0xFFFFFFFF)
		end
		-- REM: Signed remainder
		cpu.mdr6 = function (a, b)
			if b == 0 then return a end
			a, b = signed(a), signed(b)
			-- modtest.c results imply:
			-- 1. The sign of the remainder is always the same as A's sign.
			-- 2. The absolute value of the remainder is independent of any sign.
			-- Thus:
			local abr = math.abs(a) % math.abs(b)
			if a < 0 then
				abr = -abr
			end
			return bitBand(abr, 0xFFFFFFFF)
		end
		-- REMU: Unsigned remainder
		cpu.mdr7 = function (a, b)
			if b == 0 then return a end
			return bitBand(a % b, 0xFFFFFFFF)
		end
		local oldalu = cpu.compileALU
		cpu.compileALU = function (funct3, funct7, rs1cc, rs2cc, actimm)
			if funct7 == 1 and (undocumented or not actimm) then
				return "cpu.mdr" .. funct3 .. "(" .. rs1cc .. ", " .. rs2cc .. ")"
			end
			return oldalu(funct3, funct7, rs1cc, rs2cc)
		end
	end
}
