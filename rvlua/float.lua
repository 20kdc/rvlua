--[[
 rvlua - Attempt to efficiently run RISC-V code in Lua.
 Written starting in 2018 by contributors (see CONTRIBUTORS.txt).
 To the extent possible under law, the author(s) have dedicated all copyright and related and neighboring rights to this software to the public domain worldwide. This software is distributed without any warranty.
 You should have received a copy of the CC0 Public Domain Dedication along with this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
--]]

local rvlua = require("rvlua")
local bit32 = rvlua.get_bit32()
local fpack = require("rvlua.fpack")

return {
	extendF = function (cpu)
		-- For simplicity's sake, the floating-point registers don't have reguse entries.
		cpu.fpack = fpack
		cpu.fregs = {}
		cpu.fwids = {}
		for i = 0, 31 do
			cpu.fregs[i] = 0
			cpu.fwids[i] = 2
		end
		cpu.fRound = function (rm, f)
			if rm == 7 then
				-- 'dynamic rounding mode'???
			end
			if rm == 0 then
				-- RNE
				return math.floor(f + 0.5)
			elseif rm == 1 then
				-- RTZ
				if f < 0 then
					return math.ceil(f)
				end
				return math.floor(f)
			elseif rm == 2 then
				-- RDN
				return math.floor(f)
			elseif rm == 3 then
				-- RUP
				return math.ceil(f)
			elseif rm == 4 then
				-- RMM
				return math.floor(f + 0.5)
			elseif rm == 5 then
				-- INVALID
				return math.floor(f)
			elseif rm == 6 then
				-- INVALID
				return math.floor(f)
			elseif rm == 7 then
				-- INVALID
				return math.floor(f)
			end
		end
		cpu.fGet2 = function (rs)
			if cpu.fwids[rs] == 2 then
				return cpu.fregs[rs]
			else
				return 0/0
			end
		end
		cpu.fLW2 = function (rd, addr)
			local v = cpu.memory.memoryG32(addr)
			if not v then return end
			cpu.fregs[rd] = fpack.unpack(v)
			cpu.fwids[rd] = 2
			return true
		end
		cpu.fcsr = 0
		cpu.opcomps[0x07] = function (reguse, ins)
			local rd, funct3, rs1, imm = rvlua.decodeIType(ins)
			if funct3 == 2 then
				local rs1pf, rs1cc = cpu.compileRegGet(reguse, rs1)
				local code = rs1pf .. "_ = " .. cpu.compilePIM(rs1cc, imm) .. " "
				code = code .. "if not cpu.fLW2(" .. rd .. ", _) then "
				code = code .. cpu.compileFlush(reguse, true, true)
				code = code .. "cpu.trap.access.dataG32(_) "
				code = code .. "return "
				code = code .. "end "
				cpu.advancePC(reguse)
				return code
			else
				return cpu.compileSubopTrap(reguse, ins, "\"LoadFloat Family " .. funct3 .. "\"")
			end
		end
		cpu.opcomps[0x53] = function (reguse, ins)
			local rd, funct3, rs1, rs2, funct7 = rvlua.decodeRType(ins)
			if funct7 == 0x08 then
				-- FMUL.S (ignores RM)
				cpu.advancePC(reguse)
				return "cpu.fregs[" .. rd .. "] = cpu.fGet2(" .. rs1 .. ") * cpu.fGet2(" .. rs2 .. ") cpu.fwids[" .. rd .. "] = 2 "
			elseif funct7 == 0x60 then
				if rs2 == 0 then
					-- FCVT.W.S
					local rdpf, rdcc = cpu.compileRegSet(reguse, rd)
					local code = rdpf
					code = code .. rdcc .. " = bitBand(cpu.fRound(" .. funct3 .. ", cpu.fGet2(" .. rs1 .. ")), 0xFFFFFFFF)"
					cpu.advancePC(reguse)
					return code
				end
			elseif funct7 == 0x70 then
				local fpackfunc
				if rs2 == 0 and funct3 == 0 then
					-- FMV.X.W
					fpackfunc = "pack"
				elseif rs2 == 0 and funct3 == 1 then
					-- FCLASS.S
					fpackfunc = "fclass"
				end
				if fpackfunc then
					local rdpf, rdcc = cpu.compileRegSet(reguse, rd)
					local code = rdpf
					code = code .. rdcc .. " = cpu.fpack." .. fpackfunc .. "(cpu.fGet2(" .. rs1 .. ")) "
					cpu.advancePC(reguse)
					return code
				end
			elseif funct7 == 0x78 then
				if rs2 == 0 and funct3 == 0 then
					-- FMV.W.X
					local rs1pf, rs1cc = cpu.compileRegGet(reguse, rs1)
					cpu.advancePC(reguse)
					return rs1pf .. "cpu.fregs[" .. rd .. "] = cpu.fpack.unpack(" .. rs1cc .. ") cpu.fwids[" .. rd .. "] = 2 "
				end
			end
			return cpu.compileSubopTrap(reguse, ins, "\"FPU Family " .. funct7 .. "\"")
		end
	end
}
