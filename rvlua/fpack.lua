--[[
 rvlua - Attempt to efficiently run RISC-V code in Lua.
 Written starting in 2018 by contributors (see CONTRIBUTORS.txt).
 To the extent possible under law, the author(s) have dedicated all copyright and related and neighboring rights to this software to the public domain worldwide. This software is distributed without any warranty.
 You should have received a copy of the CC0 Public Domain Dedication along with this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
--]]

-- Float packer/unpacker.
-- Works with 32-bit integers.

local exponents = {}
local min = 0
local step = 0.0000000000000000000000000000000000000000000014012984643248170709237295832899161312802619418765157717570682838897910826858606014866381883621215820312
for exp = 0, 254 do
	exponents[exp] = {min, step}
	-- Self-test
	if exp == 1 then
		assert(min == 0.000000000000000000000000000000000000011754943508222875079687365372222456778186655567720875215087517062784172594547271728515625, "Self-test @ e1 failed, min was " .. min)
	end
	if exp == 2 then
		assert(min == 0.00000000000000000000000000000000000002350988701644575015937473074444491355637331113544175043017503412556834518909454345703125, "Self-test @ e2 failed, min was " .. min)
	end
	if exp == 0x7B then
		assert(min == 0.0625, "Self-test @ e0x7B failed, min was " .. min)
		assert((min + step) == 0.062500007450580596923828125, "Self-test @ e0x7B failed (rs: " .. (min + step) .. ")")
	end
	-- This is weird...
	if exp > 0 then
		step = step * 2
	end
	min = step * 0x800000
end

local function nan(f)
	-- a NaN never equals itself.
	-- (0/0) == FFC00000
	-- -(0/0) == 7FC00000
	return f ~= f
end

return {
	nan = nan,
	pack = function (f)
		local sign = f < 0
		if sign then f = -f end
		if nan(f) then
			if sign then
				return 0xFFC00000
			else
				return 0x7FC00000
			end
		end
		-- Not a NaN, is it inf?
		if f == math.huge then
			if sign then
				return 0xF8000000
			else
				return 0x78000000
			end
		end
		local exp = 0
		local min, step
		while exp < 255 do
			min, step = (unpack or table.unpack)(exponents[exp])
			if f <= (min + (step * 0x7FFFFF)) then
				break
			end
			exp = exp + 1
		end
		local frac = 0
		-- Leave frac as 0 if this hit infinity
		if exp ~= 255 then
			frac = math.min(math.floor((f - min) / step), 0x7FFFFF)
		end
		if sign then
			return (exp * 0x800000) + frac + 0x80000000
		else
			return (exp * 0x800000) + frac
		end
	end,
	unpack = function (i)
		local sign = false
		if i >= 0x80000000 then
			sign = true
			i = i - 0x80000000
		end
		local exp = math.floor(i / 0x800000)
		local frac = i % 0x800000
		local v
		if exp == 255 then
			if frac == 0 then
				v = math.huge
			else
				v = -(math.huge * 0) -- -nan
			end
		else
			local min, step = (unpack or table.unpack)(exponents[exp])
			v = min + (step * frac)
		end
		if sign then
			v = -v
		end
		return v
	end,
	fclass = function (f)
		if f < 0 then
			if f == math.huge then
				return 1
			elseif f == -0.0 then
				return 8
			elseif nan(f) then
				return 512 -- Quiet NaN
			elseif f > -exponents[1][1] then
				-- Subnormal
				return 4
			end
			-- Negative normal number
			return 2
		else
			if f == math.huge then
				return 128
			elseif f == 0.0 then
				return 16
			elseif nan(f) then
				return 512 -- Quiet NaN
			elseif f < exponents[1][1] then
				-- Subnormal
				return 32
			end
			-- Positive normal number
			return 64
		end
	end
}
