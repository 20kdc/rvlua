--[[
 rvlua - Attempt to efficiently run RISC-V code in Lua.
 Written starting in 2018 by contributors (see CONTRIBUTORS.txt).
 To the extent possible under law, the author(s) have dedicated all copyright and related and neighboring rights to this software to the public domain worldwide. This software is distributed without any warranty.
 You should have received a copy of the CC0 Public Domain Dedication along with this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
--]]

-- Adapts aligned get32le/set32le primitives into the full gamut expected by RVLua.
return function (rda32, wra32)
	local function split32(v)
		return v % 0x100,
			math.floor(v / 0x100) % 0x100,
			math.floor(v / 0x10000) % 0x100,
			math.floor(v / 0x1000000) % 0x100
	end
	local function join32(a, b, c, d)
		if not (a and b and c and d) then return end
		return a + (b * 0x100) + (c * 0x10000) + (d * 0x1000000)
	end

	local function rd8(k)
		local aa = math.floor(k / 4) * 4
		local av = rda32(aa)
		if not av then return end
		return select((k % 4) + 1, split32(av))
	end
	local function wr8(k, v)
		local aa = math.floor(k / 4) * 4
		local av = rda32(aa)
		if not av then return end
		local dat = {split32(av)}
		dat[(k % 4) + 1] = v
		return wra32(aa, join32((unpack or table.unpack)(dat)))
	end

	local function rd16(k)
		if k % 4 == 0 then
			return rda32(k) % 0x10000
		end
		return join32(rd8(k), rd8(k + 1), 0, 0)
	end
	local function wr16(k, v)
		local a, b, c, d = split32(v)
		return wr8(k, a) and wr8(k + 1, b)
	end

	local function rd32(k)
		if k % 4 == 0 then
			return rda32(k)
		end
		return join32(rd8(k), rd8(k + 1), rd8(k + 2), rd8(k + 3))
	end
	local function wr32(k, v)
		if k % 4 == 0 then
			return wra32(k, v)
		end
		local a, b, c, d = split32(v)
		return wr8(k, a) and wr8(k + 1, b) and wr8(k + 2, c) and wr8(k + 3, d)
	end

	return rd32, rd16, rd8, wr32, wr16, wr8
end
