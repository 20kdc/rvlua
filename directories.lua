--[[
 rvlua - Attempt to efficiently run RISC-V code in Lua.
 Written starting in 2018 by contributors (see CONTRIBUTORS.txt).
 To the extent possible under law, the author(s) have dedicated all copyright and related and neighboring rights to this software to the public domain worldwide. This software is distributed without any warranty.
 You should have received a copy of the CC0 Public Domain Dedication along with this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
--]]

-- Is this even copyrightable? I sure hope not.
if _VERSION == "Lua 5.1" or _VERSION == "Lua 5.2" then
 package.path = "./?/init.lua;" .. package.path
end
