#!/bin/sh

echo lua5.2
time lua5.2 emu_riscv.lua leap < testscript.txt > /dev/null
echo lua5.3
time lua5.3 emu_riscv.lua leap < testscript.txt > /dev/null
echo luajit
time luajit emu_riscv.lua leap < testscript.txt > /dev/null
echo "luajit uleap (cached steps)"
time luajit emu_riscv.lua uleap < testscript.txt > /dev/null
