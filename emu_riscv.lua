--[[
 rvlua - Attempt to efficiently run RISC-V code in Lua.
 Written starting in 2018 by contributors (see CONTRIBUTORS.txt).
 To the extent possible under law, the author(s) have dedicated all copyright and related and neighboring rights to this software to the public domain worldwide. This software is distributed without any warranty.
 You should have received a copy of the CC0 Public Domain Dedication along with this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
--]]

local mode, fn = ...
local co, trigger
if mode == "leap" then
	trigger = function ()
		while true do
			co.leap(128, 4) -- seems to be near-optimal
		end
	end
elseif mode == "leapdebug" then
	trigger = function ()
		while true do
			co.leap(128, 4, true)
		end
	end
elseif mode == "uleap" then
	trigger = function ()
		while true do
			co.leap(1, 0)
		end
	end
elseif mode == "step" then
	trigger = function ()
		while true do
			co.step(true)
		end
	end
else
	error("Unknown mode " .. mode .. " try leap/leapdebug/uleap/step")
end

-- Testbed.
-- Needs bitops from Vifino's "lua-cpuemus" repository,
--  but it can work with the bit32 library or a correctly wrapped LuaJIT bit library.
-- If using the LuaJIT bit library, please ensure to wrap to fix these:
--[[
 bitops: Logic functions are SIGNED (not unsigned) 32-bit. Really. Most likely LuaJIT. Patching.
 bitops: arshift wraps at 32, patched.
 bitops: arshift can't handle negative shifts, patched.
 bitops: rshift wraps at 32, patched.
 bitops: rshift can't handle negative shifts, patched.
 bitops: lshift wraps at 32, patched.
 bitops: lshift can't handle negative shifts, patched.
--]]
-- (This should explain why the bitops library is needed for at least LuaJIT)

require("directories")

local bops = bit32
local ok, e = pcall(function ()
 bops = require("bitops")
end)
if not ok then io.stderr:write(tostring(e) .. "\n") end
assert(bops, "No bitops available - get vifino's from lua-cpuemus, or use Lua 5.2+")

local rv = require("rvlua")
local rvf = require("rvlua.float")
local rva = require("rvlua.adapter")
rv.set_bit32(bops)

local memory = {}

local function rda32(k)
	if k == 0x80000000 then
		return (io.read(1) or ""):byte() or 0
	end
	return assert(memory[k], "bad memory access " .. k)
end
local function wra32(k, v)
	if k == 0x80000000 then
		if v == 0 then os.exit(0) end
		io.write(string.char(v))
		io.flush()
		return
	end
	assert(memory[k], "bad memory access " .. k)
	memory[k] = v
end

local rd32, rd16, rd8, wr32, wr16, wr8 = rva(rda32, wra32)

local f = io.open(fn or "testbed/riscv-test.bin", "rb")
local mc = f:read("*a")
for i = 0, 0xFFFF, 4 do
	memory[i] = 0
end
for i = 0, 0xFFFF do
	wr8(i, mc:byte(i + 1) or 0)
end
f:close()

-- defined above for trigger
co = rv.new({
	memlibMode = true,
	instretCycle = false
}, function (k)
	if k % 4 ~= 0 then error("unaligned instruction access") end
	return assert(memory[k], "out of bounds instruction access")
end, rd32, rd16, rd8, wr32, wr16, wr8)

-- Allow the undocumented accidents
rv.extendM(co, true)
rvf.extendF(co)

co.regs[2] = 0x10000
trigger()
