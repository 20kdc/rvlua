# RVLua: Embeddable VM for RISC-V code

RVLua is an embeddable VM implementing the RV32IM user specification (without the privileged ISA) in Lua.

It depends on bit32 (use vifino's lua-cpuemus bitops library for LuaJIT support).

The primary purpose of it is a way to execute arbitrary sandboxed C/C++ code in a relatively efficient manner.

## Implementation Coverage

The privileged spec is simply not implemented.

This is in line with other "light" implementations such as cliffordwolf's PicoRV32,
 where the CPU only has to strictly follow the user-level ISA to be useful,
 with support for a major operating system being secondary or pointless for the intended usage.

As for rvlua.float, please see Implementation Quirks/rvlua.float, which describes the RVLua FPU and why you shouldn't use it.

## Implementation Quirks

### Hooks

RVLua is extended by hooking any function you want to change the behavior of.

There are small exceptions to this.

As an example, if implementing the C extension (compressed instruction set),
 you would firstly set all of the relevant opcode callbacks (those whose lower 2 bits weren't both 1) in `cpu.opcomps` to your callback.

Your callback would do translation, set pcAdvance to 2 (this is reset to 4 *before* every instruction in compileThread),
 then call the correct opcode.

The 'reguse' structure manages the compiler state, and when cpu.advancePC(reguse) is called, it would advance by 2 rather than 4,
 thus ensuring the next instruction / link register value was baked into compiled code properly.

For an ALU extension however, hooking compileALU offers control of the ALU independent of if the instruction is immediate or not.

(As accidentally achieved with the M extension, if an instruction is in the right place, it can have an immediate form created 
 merely by being implemented at all.)

Meanwhile, for a CSRR, hooking compileCSRR offers control of that.

Alternatively, hooking the CSR traps offers less control in return for easier programming.

### Traps

RVLua does lots of caching to try and reduce CPU overhead.

An exception to this is when a trap occurs.

A trap is meant for the emulated CPU's environment.

As with hooks, you replace these as you wish - unlike with hooks, these have a very explicit and consistent set of conditions.

When a trap occurs, the current compiled function ends immediately. All registers are flushed before the trap.

For only the CSR traps, the PC is advanced, and even then the return value of the csrS32 trap can override the PC.

(NOTE: If you for some reason want a csrG32 trap to change the PC, consider that your leap/step call will immediately return after 
 the trap is over. Alternatively, hook compileCSRR.)

### Instruction Cache

RVLua has an infinite instruction cache of compiled Lua functions that spans instructions that shouldn't even have been read yet.

While as per the standard RVLua assumes the non-branching path is most likely if branch compilation isn't on,
 RVLua will compile branches if you let it (in which case further optimization is down to the Lua implementation,
 but branches are written such that the 'if' content is the taken branch)

In theory this allows for maximum-performance code, especially once LuaJIT gets at it -
 in practice it seems things need tuning.

If you don't like any of this, and don't mind losing the reason RVLua is built the way it is, here's what to do:

Use `cpu.step()`.

This compiles a single instruction, runs it, and discards it, without any caching.

Doing even the semi-cached equivalent `cpu.leap(1, 0)` on LuaJIT near-doubles the time taken on my current testbench,
 so do NOT use this if trying to make Minetest stuff or somesuch.

Repeat: do NOT use this if trying to make Minetest stuff.

Instead, flush the instruction cache with `cpu.icache = {}` when necessary, and if you need any particular level of control over how 
 many instructions can be compiled, use the tuning parameters to cpu.leap (these will only affect newly compiled code though)

### Performance Notes

RVLua prefers long unbranching sequences of instructions that don't trap and *preferably* don't access memory.

In the order of least to most performance problematic, memory access, branches, traps.

### Undocumented Instructions

The RVLua ALU's M extension handling has a few 'undocumented instructions' due to the RISC-V 
 specification *implying* via coincident instruction formats a simple implementation for the immediate opcodes.

Specifically, instructions I'll nickname 'RVL.MULHI' and 'RVL.DIVUI'
 exist in RVLua if you enable them. They're now disabled by default.

As the ALU immediate instructions allowing control of funct7 are those with a funct3 of 0b001 (SLLI) or 0b101 (SRLI/SRAI),
 this is limited to the M-extension instructions with those funct3 values, which are MULH and DIVU.

I'm somewhat curious as to if any other vendors accidentally support this.
 (I know PicoRV32 doesn't due to the way it implements the M extension as a coprocessor)

A binary encoding of a particular instruction, unsigned divide x10 (a0) by 2, would be:

    ----+++ +---- ++++- --- ++++- ---++++
    0000001 00010 01010 101 01010 0010011
    FUNCT7. IMMR2 RS1.. FT3 RD... OPCODE.
      EXT-M     2   X10 SR*   X10 ALU-IMM

For quick inclusion in C programs with no memory protection for testing, the following ABI-compliant function is provided:

    0x02255513
    0x00008067

(Not totally certain about this BTW. I recommend trying this on any RISC-V processor you don't particularly mind lighting on fire.)

Assuming a cleanly muxed ALU with in-built or simulated in-built support (the way RVLua does it) for the M extension,
 the RVL.MULHI and RVL.DIVUI instructions are created by a lack of restrictions.

### rvlua.float

The current status of the RVLua FPU is that it isn't even barely usable yet.

It supports a few bare instructions in what amounts to badly implemented clones of the rounding modes,
 the register access isn't and won't be optimized, and essentially this should only be used even as a base only if absolutely necessary.

Even the instructions for transfer from internal words to the FPU are not bit-exact due to the 'recoding'...
 into an entirely different format: Lua numbers.

It is at best a basis for an implementation that will acceptably run most meaningful programs.

It is NOT even an *attempt* at a compliant implementation, and won't ever be in the form I have written it.

The RVLua FPU is absolutely not bit-exact, nor is it meant to be.
If you want an accurate FPU, use software routines, or replace the implementation entirely.

Ideally: Use LuaJIT, and use FFI access - yes, FFI access - to babysit the x86 FPU.

This will be much more performant than writing float manipulation yourself,
 and probably not that much less performant than the ideal version of my totally uncompliant FPU.

Lua is not suited for any bit-exact transforms on floats or doubles.
