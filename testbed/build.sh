RVPFX=riscv64-unknown-elf
RVGCC=$RVPFX-gcc
RVGCCOPT="-fno-inline -mabi=ilp32 -march=rv32imf -D YUP_RISCV -O4 -N -nostdlib"
RVLD=$RVPFX-ld
RVSTRIP=$RVPFX-strip
RVOBJCOPY=$RVPFX-objcopy
RVOBJDUMP=$RVPFX-objdump

RVSEC=testbed

$RVGCC $RVGCCOPT -S $RVSEC/main.c -o $RVSEC/riscv-main.S
$RVGCC $RVGCCOPT -c $RVSEC/pre.S -o $RVSEC/riscv-pre.o
$RVGCC $RVGCCOPT -c $RVSEC/main.c -o $RVSEC/riscv-main.o

# Host GCC for comparison
gcc $RVSEC/main.c -o $RVSEC/host-riscv-main

$RVLD $RVSEC/riscv-pre.o $RVSEC/riscv-main.o -melf32lriscv -T $RVSEC/work.ld -o $RVSEC/riscv-test.elf

$RVSTRIP -US -R .note.gnu.build-id -R .comment $RVSEC/riscv-test.elf
$RVOBJDUMP -d $RVSEC/riscv-test.elf > $RVSEC/testnt
$RVOBJCOPY --set-start 0 -O binary $RVSEC/riscv-test.elf $RVSEC/riscv-test.bin
