/*
 * rvlua - Attempt to efficiently run RISC-V code in Lua.
 * Written starting in 2018 by contributors (see CONTRIBUTORS.txt).
 * To the extent possible under law, the author(s) have dedicated all copyright and related and neighboring rights to this software to the public domain worldwide. This software is distributed without any warranty.
 * You should have received a copy of the CC0 Public Domain Dedication along with this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
 */

// Used on a host computer for illustrative purposes only.
// Not meant as a cross-reference.
void results(int a, int b) {
 printf("%i / %i = %ir%i\n", a, b, a / b, a % b);
}
void main() {
 for (int i = 0; i < 10; i++) {
  results(i, 10);
  results(i, -10);
  results(-i, 10);
  results(-i, -10);
 }
}
