/*
 * rvlua - Attempt to efficiently run RISC-V code in Lua.
 * Written starting in 2018 by contributors (see CONTRIBUTORS.txt).
 * To the extent possible under law, the author(s) have dedicated all copyright and related and neighboring rights to this software to the public domain worldwide. This software is distributed without any warranty.
 * You should have received a copy of the CC0 Public Domain Dedication along with this software. If not, see <http://creativecommons.org/publicdomain/zero/1.0/>.
 */

// NOTE! Must compile on host too for multiply opcode test
// pre.S / environment
int getchar();
int putchar(int ch);
// --
void puti(int ch);
void putsr(char * chs);

char mema[256];
char memb[256];

float target = 3.14159;

void * memcpy(void * dst, void * src, unsigned long sz) {
 void * olddst = dst;
 while (sz > 4) {
  *((int*)dst) = *((int*)src);
  src += 4;
  dst += 4;
  sz -= 4;
 }
 while (sz) {
  *((char*)dst) = *((char*)src);
  src++;
  dst++;
  sz--;
 }
 return olddst;
}

void main() {
 int prompt = 1;
 while (1) {
  if (prompt)
   putsr("Enter command\n");
  prompt = 1;
  int r = getchar();
  if (r == 0) {
   putsr("Goodbye\n");
   putchar(0);
  } else if (r == 'i') {
   int it = 0;
   while (it <= 10000) {
    putsr("Iteration: ");
    puti(it++);
    putchar(10);
   }
#ifdef YUP_RISCV
  } else if (r == 'w') {
   putsr("Testing if this implementation has undocumented ALU immediates\n");
   int wtfs[] = {
    // Undocumented ALU immediate
    0x02255513,
    0x00008067
   };
   int (*wtf)(int) = (void *) wtfs;
   for (int i = -16; i <= 16; i++) {
    putchar('(');
    puti(i);
    putsr("/2) = ");
    __asm("FENCE.I");
    puti(wtf(i));
    putchar(10);
   }
#endif
  } else if (r == 'c') {
   putsr("Copying memory...\n");
   for (int j = 0; j < 100; j++) {
    puti(j);
    putchar('%');
    putchar(10);
    memcpy(memb, mema, 256);
    memcpy(mema, memb, 256);
   }
  } else if (r == 'f') {
   // Converts a float to an int to test basic floating-point ops.
   // This is expected to fail until long into the future.
   float t = target;
   for (int i = 0; i < 4; i++) {
    puti((int) t);
    putchar(10);
    t *= 10;
   }
  } else if ((r != 13) && (r != 10)) {
   putsr("Unknown command\n");
  } else {
   prompt = 0;
  }
 }
}

void putic(int n);
// Takes a negative number
void putic(int n) {
 int nx = n / 10;
 int np = 1;
 while (nx) {
  nx /= 10;
  np *= 10;
 }
 do {
  putchar('0' - ((n / np) % 10));
  np /= 10;
 } while (np);
}
void puti(int n) {
 if (n < 0) {
  putchar('-');
  putic(n);
 } else {
  putic(-n);
 }
}
void putsr(char * chs) {
 while (*chs)
  putchar(*(chs++));
}
